/**
  ******************************************************************************
  * @file    main.c
  * @author  Fernando Ferreira Silva
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32l0xx.h"
#include "stm32l0xx_nucleo.h"
			
int main(void)
{
	HAL_Init();
	BSP_LED_Init(LED_GREEN);
	for(;;){
		BSP_LED_Toggle(LED_GREEN);
		HAL_Delay(500);
	}
}
